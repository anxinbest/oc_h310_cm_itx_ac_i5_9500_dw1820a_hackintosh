# oc_h310_cm_itx_ac_i5_9500_dw1820a_hackintosh

#### Description
Hachintosh for H310cm ITX/ac with Open Core
系统        macOS 10.15.6
处理器     intel i5  9500
主板        H310CM ITX/ac
内存        阿斯加特  DDR4-2666 8x2G
硬盘        阿斯加特  Nvme M2.0 256G
显卡        UHD630
声卡        Realtek ALC887 (AppleALC & Lilu)
网卡        博通DW1820A免驱

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
